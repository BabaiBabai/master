"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const controller1_1 = __importDefault(require("./controllers/controller1"));
const controller2_1 = __importDefault(require("./controllers/controller2"));
const path_1 = __importDefault(require("path"));
const body_parser_1 = __importDefault(require("body-parser"));
const file_upload_controller_1 = __importDefault(require("./controllers/file-upload-controller"));
const user_controller_1 = __importDefault(require("./controllers/user-controller"));
const mongoose_1 = __importDefault(require("mongoose"));
const RegisterController_1 = __importDefault(require("./controllers/RegisterController"));
const note_controller_1 = __importDefault(require("./controllers/note-controller"));
// import { clientApiKeyValidation,isNewSessionRequired, isAuthRequired, generateJWTToken, verifyToken } from './common/authUtils';
// import {MongoClient} from 'mongodb';
// import jwt from './_helpers/jwt';
const port = 30024;
// mongodb://username:password@host:port/dbname
const app = express_1.default();
var cors = require('cors');
app.use(body_parser_1.default.urlencoded({ extended: true }));
//need to pass extended true so that it can pass multiple form data like file upload
app.use(body_parser_1.default.json());
app.use(cors());
// use JWT auth to secure the api
// console.log('use jwt');
// app.use(jwt());
//for exposing the upload folder to see the images from browser
//app.use(express.static('./uploads'));
//we can map upload folder to some specific path name like files in below line
app.use('/files', express_1.default.static('./uploads'));
app.use('/', express_1.default.static('./front-end'));
app.set('views', path_1.default.join(__dirname, 'views'));
//to inlude views in the current directory
app.set('view engine', 'ejs');
// mongoose.connect('mongodb://localhost:27017/todoapp',{useNewUrlParser:true,useUnifiedTopology: true},(error)=>{
mongoose_1.default.connect('mongodb://admin:UKeDe1y45vDN9M05', { useNewUrlParser: true, useUnifiedTopology: true }, (error) => {
    if (error) {
        console.log(error);
    }
    else {
        console.log('connected to mongo db start');
        require('./models/user-model.js');
        require('./models/note-model.js');
        require('./models/noteModel.js');
        require('./models/userModel.js');
        console.log('connected to mongo db end');
    }
});
app.listen(port, () => {
    console.log('server created on port', { port });
});
app.use(controller1_1.default);
app.use(controller2_1.default);
app.use(file_upload_controller_1.default);
app.use(user_controller_1.default);
console.log('RegisterController start');
app.use(RegisterController_1.default);
app.use(note_controller_1.default);
app.use('*', express_1.default.static('./front-end'));
//i am pointing all invalid routed to node js
// app.get('/',(req,res)=>{
//    // res.send('Hello World');
//    console.log('In route/');
//    res.json({hello:'world'});
// })
