"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const userSchema = new mongoose_1.default.Schema({
    firstName: mongoose_1.default.Schema.Types.String,
    lastName: mongoose_1.default.Schema.Types.String,
    email: mongoose_1.default.Schema.Types.String,
    mobile: mongoose_1.default.Schema.Types.Number,
    password: mongoose_1.default.Schema.Types.String,
    profileImage: mongoose_1.default.Schema.Types.String
});
userSchema.virtual('name').get(function () {
    // @ts-ignore
    return this.firstName + ' ' + this.lastName;
});
userSchema.methods.eligibe = function () {
    return this.age >= 20;
};
userSchema.methods.toResponse = function () {
    const name = this.name;
    const data = this.toObject();
    data.id = data._id;
    data.name = name;
    delete data._id;
    delete data.__v;
    return data;
};
// userSchema.statics.getUserByUserNameAndPassword = function(userName: string, password: string) {
//   const model: IUserModel = this;
//   return model.findOne({userName, password});
// }
userSchema.statics.getUserByEmailAndPassword = function (email, password) {
    const model = this;
    return model.findOne({ email, password });
};
exports.default = mongoose_1.default.model('User', userSchema);
//# sourceMappingURL=user-model.js.map