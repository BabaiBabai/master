"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const noteSchema = new mongoose_1.default.Schema({
    title: mongoose_1.default.Schema.Types.String,
    body: mongoose_1.default.Schema.Types.String,
    author: { type: mongoose_1.default.Schema.Types.ObjectId, ref: 'User' },
    noteColor: mongoose_1.default.Schema.Types.String
});
noteSchema.statics.findAllNotes = function () {
    return __awaiter(this, void 0, void 0, function* () {
        const model = this;
        const notes = yield model.find().populate('author');
        const notesObj = notes.map(n => {
            let name = null;
            if (n.author) {
                name = n.author.name;
            }
            const nn = n.toObject();
            if (nn.author) {
                nn.author = name;
            }
            return nn;
        });
        return notesObj;
    });
};
noteSchema.statics.findNotesByAuther = function (autherId) {
    const model = this;
    return model.find({ author: autherId }).populate('author');
};
noteSchema.statics.findNotesByNoteId = function (noteid) {
    const model = this;
    return model.find({ _id: noteid }).populate('author');
};
exports.default = mongoose_1.default.model('Note', noteSchema);
//# sourceMappingURL=note-model.js.map