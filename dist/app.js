"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const controller1_1 = __importDefault(require("./controllers/controller1"));
const controller2_1 = __importDefault(require("./controllers/controller2"));
const path_1 = __importDefault(require("path"));
const body_parser_1 = __importDefault(require("body-parser"));
const file_upload_controller_1 = __importDefault(require("./controllers/file-upload-controller"));
const user_controller_1 = __importDefault(require("./controllers/user-controller"));
const mongoose_1 = __importDefault(require("mongoose"));
const cors_1 = __importDefault(require("cors"));
const port = 30024;
const app = express_1.default();
// const cors = require('cors');
// use it before all route definitions
// app.use(cors({origin: 'http://localhost:8888'}));
// const cors = require('cors');
// app.use(cors());
// const cors = require('cors');
app.use(body_parser_1.default.urlencoded({ extended: true }));
app.use(body_parser_1.default.json());
app.use('/files', express_1.default.static('./uploads'));
app.set('views', path_1.default.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.listen(port, () => {
    console.log(`Server created on port: ${port}`);
});
// app.options('*', cors())
//  app.use((req, res, next)=> {
//   res.header('Access-Control-Allow-Origin', 'http://localhost:3000');
//   res.header('Access-Control-Allow-Credentials', 'true');
//   res.header('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT');
//   // res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
//    res.header('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
//   next();
// });
app.use(cors_1.default({ origin: 'http://15.207.8.251:30024' }));
app.use(controller1_1.default);
app.use(controller2_1.default);
app.use(file_upload_controller_1.default);
app.use(user_controller_1.default);
//app.use('*',express.static('./front-end'));
app.use('/', express_1.default.static('./front-end'));
// app.use('*', express.static(path.join(__dirname, 'front-end')))
// mongoose.connect('mongodb://localhost:27017/todoapp', {useNewUrlParser: true}, (error) => {
mongoose_1.default.connect('mongodb://admin:UKeDe1y45vDN9M05@15.207.8.251:27017/?authSource=admin&w=1', { useNewUrlParser: true, useUnifiedTopology: true }, (error) => {
    if (error) {
        console.log(error);
    }
    else {
        console.log('Connected to MongoDB11');
        // import schema
        require('./models/user-model.js');
        require('./models/note-model.js');
    }
});
//# sourceMappingURL=app.js.map