"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const mongoose_1 = __importDefault(require("mongoose"));
const multer_1 = __importDefault(require("multer"));
const uuid_1 = require("uuid");
const sharp_1 = __importDefault(require("sharp"));
const path_1 = __importDefault(require("path"));
const fs_1 = __importDefault(require("fs"));
const util_1 = require("../utils/util");
const userController = express_1.default();
const storage = multer_1.default.diskStorage({
    destination: (req, file, cb) => {
        if (file.mimetype === 'image/png') {
            cb(null, './uploads');
        }
        else {
            cb(Error('File type not allowed'), '');
        }
    },
    // file.1.png -> [file - 0, 1 - 1, png - 2] -> 3 -> 3-1 = 2
    filename: (req, file, cb) => {
        const fileName = file.originalname;
        const fileParts = fileName.split(/\./g);
        const ext = fileParts[fileParts.length - 1];
        cb(null, uuid_1.v4() + '.' + ext);
    }
});
const uploader = multer_1.default({ storage });
// list of all users
userController.get('/users', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log(1);
    const userModel = mongoose_1.default.model('User');
    const dbUsers = yield userModel.find();
    res.json(dbUsers.map(i => i.toResponse()));
}));
// return a specific user
// users/1
// users/2
// undefined, null, 0, '', false    => false
userController.get('/users/:id', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log(2);
    const userModel = mongoose_1.default.model('User');
    const dbUser = yield userModel.findById(req.params.id);
    res.json(util_1.Util.cleanOutput(dbUser));
}));
userController.post('/users', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log(3);
    const userModel = mongoose_1.default.model('User');
    yield userModel.create(req.body);
    res.json({ status: true });
}));
userController.post('/users/authenticate', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log(4);
    const model = mongoose_1.default.model('User');
    console.log(req.body);
    const user = yield model.getUserByEmailAndPassword(req.body.userName, req.body.password);
    console.log(user);
    // res.json(user.eligibe());
    // const isValid = user===null?false:true;
    // if(user!==null){
    //   const jwt = require('njwt')
    //   const claims = { UserId: user.id, Email: 'AzureDiamond' }
    //   const token = jwt.create(claims, 'top-secret-phrase')
    //   token.setExpiration(new Date().getTime() + 60*1000)
    //   res.send(token.compact())
    // }
    res.json(user);
}));
userController.post('/users/:id', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log(5);
    const user = req.body;
    const userModel = mongoose_1.default.model('User');
    yield userModel.updateOne({ _id: req.params.id }, user);
    res.json({ status: true });
}));
// userController.delete('/users/:id', async (req, res) => {
//   console.log('user delete call')
//   const userModel = mongoose.model('User');
//   await userModel.remove({_id: req.params.id});
//   res.json({status: true});
// });
userController.post('/users/:id/image-upload', uploader.single('image'), (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log(6);
    console.log(req.file);
    console.log('File uploaded....');
    const resizedFile = sharp_1.default(path_1.default.join(req.file.destination, req.file.filename));
    const imageName = uuid_1.v4() + '.jpg';
    yield resizedFile.resize(200).toFile(path_1.default.join(req.file.destination, imageName));
    fs_1.default.unlinkSync(path_1.default.join(req.file.destination, req.file.filename));
    const userModel = mongoose_1.default.model('User');
    yield userModel.updateOne({ _id: req.params.id }, { profileImage: imageName });
    res.send('Done');
}));
userController.get('/notes', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log(7);
    const model = mongoose_1.default.model('Note');
    const notes = yield model.findAllNotes();
    res.json(notes);
}));
userController.post('/notes', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log(8);
    console.log(req.body);
    const model = mongoose_1.default.model('Note');
    yield model.create(req.body);
    res.json({ status: true });
}));
userController.post('/UpdateNotes', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('updateNote');
    const model = mongoose_1.default.model('Note');
    const update = { title: req.body.title, body: req.body.body, noteColor: req.body.noteColor };
    const filter = { _id: req.body.id };
    ;
    yield model.findOneAndUpdate(filter, update);
    res.json({ status: true });
}));
userController.get('/users/:id/notes', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log(9);
    console.log('get users call');
    const model = mongoose_1.default.model('Note');
    const notes = yield model.findNotesByAuther(req.params.id);
    res.json(notes);
}));
userController.get('/notes/:id/notes', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const model = mongoose_1.default.model('Note');
    const notes = yield model.findNotesByNoteId(req.params.id);
    res.json(notes);
}));
userController.delete('/users/:id/notes', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log(11);
    console.log('delete call');
    console.log(req.params.id);
    const model = mongoose_1.default.model('Note');
    // const ObjectId = require('mongodb').ObjectID;
    // let hex = /[0-9A-Fa-f]{6}/g;
    // let idd = (hex.test(req.params.id))? ObjectId(req.params.id) : req.params.id;
    yield model.deleteOne({ _id: req.params.id });
    res.json({ status: true });
}));
exports.default = userController;
//# sourceMappingURL=user-controller.js.map