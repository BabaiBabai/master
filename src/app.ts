import express from 'express';
import controller1 from './controllers/controller1';
import controller2 from './controllers/controller2';
import path from 'path';
import bodyParser from 'body-parser';
import fileUploadController from './controllers/file-upload-controller';
import userController from './controllers/user-controller';
import mongoose from 'mongoose';
import cors from 'cors'

const port = 30024;

const app = express();
// const cors = require('cors');

// use it before all route definitions
// app.use(cors({origin: 'http://localhost:8888'}));
// const cors = require('cors');
// app.use(cors());
// const cors = require('cors');
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use('/files', express.static('./uploads'));


app.set( 'views', path.join( __dirname, 'views' ) );
app.set( 'view engine', 'ejs' );

app.listen(port, () => {
  console.log(`Server created on port: ${port}`);
});
 // app.options('*', cors())
//  app.use((req, res, next)=> {

//   res.header('Access-Control-Allow-Origin', 'http://localhost:3000');
//   res.header('Access-Control-Allow-Credentials', 'true');
//   res.header('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT');

//   // res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
//    res.header('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
//   next();
// });
 app.use(cors({origin: 'http://15.207.8.251:30024'}));
app.use(controller1);
app.use(controller2);
app.use(fileUploadController);
app.use(userController);
 //app.use('*',express.static('./front-end'));
 app.use('/',express.static('./front-end'));

 // app.use('*', express.static(path.join(__dirname, 'front-end')))
// mongoose.connect('mongodb://localhost:27017/todoapp', {useNewUrlParser: true}, (error) => {
  mongoose.connect('mongodb://admin:UKeDe1y45vDN9M05@15.207.8.251:27017/?authSource=admin&w=1',{useNewUrlParser:true,useUnifiedTopology: true }, (error) => {
  if (error) {
    console.log(error);
  } else {
    console.log('Connected to MongoDB11');

    // import schema
    require('./models/user-model.js');
    require('./models/note-model.js');
  }
});