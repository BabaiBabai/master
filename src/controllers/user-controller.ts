import express from 'express';
import mongoose from 'mongoose';
import multer from 'multer';
import { v4 as uuidv4 } from 'uuid';
import sharp from 'sharp';
import path from 'path';
import fs from 'fs';
import { Util } from '../utils/util';
import { IUser, IUserModel } from '../models/user-model';
import { INote, INoteModel } from '../models/note-model';

const userController = express();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    if (file.mimetype === 'image/png') {
      cb(null, './uploads');
    } else {
      cb(Error('File type not allowed'), '');
    }
  },
  // file.1.png -> [file - 0, 1 - 1, png - 2] -> 3 -> 3-1 = 2
  filename: (req, file, cb) => {
    const fileName = file.originalname;
    const fileParts = fileName.split(/\./g);
    const ext = fileParts[fileParts.length - 1];
    cb(null, uuidv4() + '.' + ext);
  }
})
const uploader = multer({storage});

// list of all users
userController.get('/users', async (req, res) => {
  console.log(1);
  const userModel = mongoose.model<IUser, IUserModel>('User');

  const dbUsers = await userModel.find();

  res.json(dbUsers.map(i => i.toResponse()));
});

// return a specific user
// users/1
// users/2
// undefined, null, 0, '', false    => false
userController.get('/users/:id', async (req, res) => {
  console.log(2);
  const userModel = mongoose.model('User');
  const dbUser = await userModel.findById(req.params.id);
  res.json(Util.cleanOutput(dbUser));
});

userController.post('/users', async (req, res) => {
  console.log(3);
  const userModel = mongoose.model('User');

  await userModel.create(req.body);

  res.json({status: true});
});




userController.post('/users/authenticate', async (req, res) => {
  console.log(4);
  const model = mongoose.model<IUser, IUserModel>('User');
  console.log(req.body);
  const user = await model.getUserByEmailAndPassword(req.body.userName, req.body.password);
 console.log(user);
 // res.json(user.eligibe());
 // const isValid = user===null?false:true;
// if(user!==null){
//   const jwt = require('njwt')
//   const claims = { UserId: user.id, Email: 'AzureDiamond' }
//   const token = jwt.create(claims, 'top-secret-phrase')
//   token.setExpiration(new Date().getTime() + 60*1000)
//   res.send(token.compact())
// }

 res.json(user);
});

userController.post('/users/:id', async (req, res) => {
  console.log(5);
  const user = req.body;
  const userModel = mongoose.model('User');

  await userModel.updateOne({_id: req.params.id}, user);

  res.json({status: true});
});

// userController.delete('/users/:id', async (req, res) => {
//   console.log('user delete call')
//   const userModel = mongoose.model('User');

//   await userModel.remove({_id: req.params.id});

//   res.json({status: true});
// });

userController.post('/users/:id/image-upload', uploader.single('image'), async (req, res) => {
  console.log(6);
  console.log(req.file);
  console.log('File uploaded....');
  const resizedFile = sharp(path.join(req.file.destination, req.file.filename));
  const imageName = uuidv4() + '.jpg'
  await resizedFile.resize(200).toFile(path.join(req.file.destination, imageName));
  fs.unlinkSync(path.join(req.file.destination, req.file.filename));

  const userModel = mongoose.model('User');
  await userModel.updateOne({_id: req.params.id}, {profileImage: imageName});

  res.send('Done');
});

userController.get('/notes', async (req, res) => {
  console.log(7);
  const model = mongoose.model<INote, INoteModel>('Note');
  const notes = await model.findAllNotes();

  res.json(notes);
});

userController.post('/notes', async (req, res) => {
  console.log(8);
  console.log(req.body);
  const model = mongoose.model<INote, INoteModel>('Note');
  await model.create(req.body);
  res.json({status: true});
});
userController.post('/UpdateNotes', async (req, res) => {
  console.log('updateNote');
  const model = mongoose.model<INote, INoteModel>('Note');
    const update = {title:req.body.title,body:req.body.body,noteColor:req.body.noteColor};
    const filter = { _id: req.body.id };;
  await model.findOneAndUpdate(filter,update);
  res.json({status: true});
});
userController.get('/users/:id/notes', async (req, res) => {
  console.log(9);
console.log('get users call');
  const model = mongoose.model<INote, INoteModel>('Note');
  const notes = await model.findNotesByAuther(req.params.id);

  res.json(notes);
});
userController.get('/notes/:id/notes', async (req, res) => {

  const model = mongoose.model<INote, INoteModel>('Note');
  const notes = await model.findNotesByNoteId(req.params.id);

  res.json(notes);
});


userController.delete('/users/:id/notes', async (req, res) => {
  console.log(11);
  console.log('delete call');
  console.log(req.params.id);
  const model = mongoose.model<INote, INoteModel>('Note');
 // const ObjectId = require('mongodb').ObjectID;
  // let hex = /[0-9A-Fa-f]{6}/g;
  // let idd = (hex.test(req.params.id))? ObjectId(req.params.id) : req.params.id;

  await model.deleteOne({_id:req.params.id});

  res.json({status: true});
});
export default userController;